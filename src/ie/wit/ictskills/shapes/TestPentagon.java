package ie.wit.ictskills.shapes;

import java.util.ArrayList;

/**
 * 
 * Task 3: A test class to display a series of Pentagon objects, referencing the Shapes
 * superclass and pentagon subclass.
 * 
 * @author skealy
 * @version 2.0 May 15, 2016
 *
 */

public class TestPentagon
{

  public static void main(String[] args)
  {
    // TODO Task 3: Display 4 cascaded Pentagons differently colored shapes.
    // (int radius, int xPosition, int yPosition, String color, boolean
    // isVisible)

    ArrayList<Shapes> shapes = new ArrayList<>();
    // super(int xPosition, int yPosition, String color, boolean isVisible)
    shapes.add(new Pentagon(30, 60, 30, "red"));
    shapes.add(new Pentagon(50, 100, 60, "blue"));
    shapes.add(new Pentagon(70, 140, 90, "green"));
    shapes.add(new Pentagon(90, 180, 120, "black"));

    shapes.add(new Pentagon(90, 0, 0, "black")); // Tests the MoveTo 0,0

    for (Shapes shape : shapes)
    {
      shape.makeVisible(); // invokes the boolean value
    }
  }

}