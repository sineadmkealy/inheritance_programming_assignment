package ie.wit.ictskills.shapes;

/**
 * An abstract class describing geometric shapes
 * 
 * @author jfitzgerald
 * @version 2016-04-12
 */
abstract public class Shapes
{
    int xPosition;
    int yPosition;
    String color; 
    boolean isVisible;
    
    
    public Shapes(int xPosition, int yPosition, String color, boolean isVisible)
    {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.color = color;
        this.isVisible = isVisible;        
    }

    public void makeVisible()
    {
        isVisible = true;
        draw();
    }

    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    public void moveTo(int x, int y) //used to implement Task 2 Pentagon
    {
        xPosition = x;
        yPosition = y;
        makeVisible();
    }
    
    // Abstract methods are only declared but  not implemented in parent Shapes class. These must be 
    // implemented in each derived subclass of Shapes:
    abstract void draw();
    abstract void changeSize(int scale);
    

    public void changeColor(String color)
    {
        this.color = color;
        draw();
    }

    protected void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
    
   
       
}
