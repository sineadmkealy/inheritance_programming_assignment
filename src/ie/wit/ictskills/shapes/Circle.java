package ie.wit.ictskills.shapes;

import java.awt.geom.Ellipse2D;

/**
 * A circle that can be manipulated and that draws itself on a canvas. Task 4
 * changes the inheritance hierarchy whereby the circle class derives from the
 * Ellipse class, which is a direct subclass of the shapes superclass
 * 
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 */

public class Circle extends Ellipse implements Measurable
// Inheritance 'extends' keyword creates the circle as a derived class from the
// parent Ellipse class
// The interface 'implements' keyword creates objects of the circle class that
// must implement
// all the methods of the Ellipse parent class
{
  /**
   * Create a new circle at default position with default color & diameter.
   * Circle class a subclass of Ellipse class
   */
  public Circle()
  {
    // Invokes super class Ellipse with xDiameter & yDiameter == 100 units
    // Default circle positioned at 0,0
    super(120, 120, 0, 0, "red", false); // (diameter, diameter, xPosition,
                                         // yPosition, color)
  }

  /**
   * Overloaded constructor for the circle shape invokes the Ellipse superclass
   */
  public Circle(int xdiameter, int ydiameter, int xPosition, int yPosition, String color)
  {
    super(xdiameter, ydiameter, xPosition, yPosition, color, true);
  }

  /**
   * New method references the Measurable interface Defines perimeter details
   * for the circle class, which override the parent class Ellipse
   */
  @Override
  public double perimeter()
  {

    return Math.PI * xdiameter;
  }

}
