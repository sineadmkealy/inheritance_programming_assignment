package ie.wit.ictskills.shapes;

// TODO Task 4: Complete Ellipse, inherit Shapes, implement Measurable, subclass Circle.
import ie.wit.ictskills.util.ellipse.EllipseMeasure;

import java.awt.geom.*;

/**
 * An ellipse that can be manipulated and that draws itself on a canvas.
 * 
 * @author
 * 
 * @version
 */

public class Ellipse extends Shapes implements Measurable
{
  protected int xdiameter;
  private int ydiameter;

  /**
   * Create a new ellipse at default position with default color.
   */
  public Ellipse() // default constructor
  {
    super(20, 60, "blue", false); // super(xPosition, yPosition, color, boolean
                                  // isVisible)
    xdiameter = 30;
    ydiameter = 60;
  }

  /**
   * Overloaded constructor for the Ellipse shape
   */
  public Ellipse(int xdiameter, int ydiameter, int xPosition, int yPosition, String color, boolean isVisible) // overloaded
                                                                                                              // constructor
  {
    super(xPosition, yPosition, color, true);
    this.xdiameter = xdiameter;
    this.ydiameter = ydiameter;
  }

  /**
   * Implements the Measurable interface Defines perimeter details for the
   * ellipse class
   */
  public double perimeter()
  {
    return EllipseMeasure.perimeter(xdiameter, ydiameter);
    // the method calls on the ellipse.jar file which contains the calculation
    // for ellipse perimeter
  }

  /**
   * Change the size to the new size (in pixels). Size must be >= 0.
   */
  @Override
  void changeSize(int scale)
  {
    // TODO Auto-generated method stub
    erase();
    this.xdiameter *= scale;
    this.ydiameter *= scale;
    draw();
  }

  /**
   * Draw the ellipse with current specifications on screen. Task 4: Details
   * migrated from the circle subclass
   */
  void draw()
  {
    if (isVisible)
    {
      Canvas canvas = Canvas.getCanvas();
      canvas.draw(this, color, new Ellipse2D.Double(xPosition, yPosition, xdiameter, ydiameter));
      canvas.wait(10);
    }
  }

  /**
   * Main method to draw an Ellipse with specified attributes * Task 4: Details
   * migrated from the circle subclass
   */
  public static void main(String[] args)
  {
    Ellipse ellipse = new Ellipse();
    ellipse.makeVisible();
    ellipse.draw();
  }
}
