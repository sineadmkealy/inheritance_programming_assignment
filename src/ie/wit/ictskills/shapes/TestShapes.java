package ie.wit.ictskills.shapes;

import java.util.ArrayList;

/**
 * A test class to instantiate circle, triangle and rectangle objects and
 * display in a cascade style
 * 
 * @author skealy
 * @version 2.0 May 15, 2016
 *
 */

// TODO Task 7: Refactor TestShapes
public class TestShapes
{
  public static void main(String[] args)
  // Reflects polymorphism in action, creating an arraylist of shapes references
  // in main, then initialising these with reference to each shape class.
  // A for-each loop used to traverse elements in the list
  // The shapes class implements makeVisible().
  {
    ArrayList<Shapes> shapes = new ArrayList<>();

    shapes.add(new Circle(30, 30, 20, 60, "red"));
    shapes.add(new Circle(40, 40, 30, 70, "blue"));
    shapes.add(new Circle(50, 50, 40, 80, "green"));
    shapes.add(new Circle(60, 60, 50, 90, "black"));

    shapes.add(new Triangle(30, 40, 160, 50, "red"));
    shapes.add(new Triangle(40, 50, 170, 60, "blue"));
    shapes.add(new Triangle(50, 60, 180, 70, "green"));
    shapes.add(new Triangle(60, 70, 190, 80, "black"));

    shapes.add(new Rectangle(120, 30, 60, 150, "red"));
    shapes.add(new Rectangle(120, 30, 70, 160, "blue"));
    shapes.add(new Rectangle(120, 30, 80, 170, "green"));
    shapes.add(new Rectangle(120, 30, 90, 180, "black"));

    for (Shapes shape : shapes)
    {
      shape.makeVisible();
    }
  }
}
