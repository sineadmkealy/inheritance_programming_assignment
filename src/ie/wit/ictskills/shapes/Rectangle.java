package ie.wit.ictskills.shapes;

/**
 * A rectangle class to inherit data and behaviour (methods) from the Shapes
 * parent class Implements the Measurable interface
 * 
 * @author skealy
 * @version 2.0 May 15, 2016
 *
 */

// TODO Task 1: Refactor: derive from Shapes super class

public class Rectangle extends Shapes implements Measurable
// Inheritance 'extends' keyword means the rectangle subclass may directly use
// working methods in the Shapes
// superclass. The subclass may also override methods in the superclass and add
// new methods and fields to the subclass.
// The interface 'implements' keyword creates objects of the rectangle class
// that must implement all methods of the
// Measurable interface, unless @override provided to the relevant method
{
  private int xSideLength;
  private int ySideLength;
  // selected fields removed where implemented in and inherited from superclass
  // Shapes
  // only rectangle-specific subclass fields declared

  /**
   * Default Constructor Creates a new rectangle at default position, with
   * default size dimensions, colour and not visible
   */
  public Rectangle()
  {
    super(60, 50, "red", false); // field vlauess declared per superclass
    xSideLength = 60; // declares field values specific to the rectangle
                      // subclass
    ySideLength = 30; // declares field values specific to the rectangle
                      // subclass
  }

  /**
   * Overloaded constructor for rectangle shape
   */
  public Rectangle(int xSideLength, int ySideLength, int xPosition, int yPosition, String color)
  {
    super(xPosition, yPosition, color, true); // rectangle initialises fields in
                                              // superclass
    this.xSideLength = xSideLength; // rectangle initialises its own field in
                                    // the subclass
    this.ySideLength = ySideLength; // rectangle initialises its own field in
                                    // the subclass
  }

  // methods setState, makeVisible, makeInvisible(), changeColour(), erase()
  // removed where
  // implemented in Superclass Shapes

  /**
   * Method to draw the rectangle shape This method overrides the abstract
   * method 'draw' in the Superclass Shapes
   */
  void draw()
  {
    if (isVisible)
    {
      Canvas canvas = Canvas.getCanvas();
      canvas.draw(this, color, new java.awt.Rectangle(xPosition, yPosition, xSideLength, ySideLength));
      canvas.wait(10);
      // the Rectangle class is located in the package java.awt.
      // contained in a Java sub-folder within the installation
    }
  }

  /**
   * Change the size to the new size (int scale) and draw the rectangle. Size
   * must be >= 0 This method overrides the abstract method 'change size' in the
   * Superclass Shapes
   */
  @Override
  void changeSize(int scale)
  {
    // TODO Auto-generated method stub
    erase();
    this.xSideLength *= scale;
    this.ySideLength *= scale;
    draw();
  }

  /**
   * New main method to draw a rectangle with specified attributes
   */
  public static void main(String[] args)
  // the parameter String[] args is used to input information to the method when
  // the application execution begins
  {
    Rectangle rectangle = new Rectangle(100, 50, 50, 100, "red");
    rectangle.makeVisible();
    rectangle.draw();
  }

  /**
   * New method references the Measurable interface Defines perimeter details
   * for the rectangle class
   */
  @Override
  public double perimeter()
  {
    return (xSideLength + xSideLength) * 2;
  }
}