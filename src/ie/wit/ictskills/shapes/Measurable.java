package ie.wit.ictskills.shapes;

/**
 * Measurable interface implements its associated methods and creates objects of
 * classes that implement the Measurable interface
 * 
 * @author skealy
 * @version 2.0 May 15, 2016
 */
public interface Measurable
// Task 5: implements the perimeter calculation in all shape classes that
// implement the interface:-
// in circle, pentagon, rectangle, triangle and ellipse classes.
// This relates to behaviour only, objects are not created of the interface
// An example of polymorphism - perimeter() method can calculate different
// calculations for perimeter depending
// on specific formula as implemented in each individual shape class
{
  double perimeter();
}