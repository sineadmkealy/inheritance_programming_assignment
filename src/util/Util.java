package util;

import ie.wit.ictskills.shapes.Measurable;

import java.util.ArrayList;

public class Util
{
  /**
   * Measureable interface contains the method double perimeter(). The method
   * maximum here evaluates the single value representing the largest perimeter
   * discovered in the list of Measurable objects.
   *
   * @param object
   *          The list of objects whose classes implement the interface
   *          Measurable
   * @return Returns the largest perimeter discovered among entire list objects.
   */
  static public double maximum(ArrayList<Measurable> shapes)
  {
    Measurable max = null;
    // TODO Task 6: Implement method Util.maximum

    // create arraylist of type shapes
    for (int i = 0; i < shapes.size(); i += 1) // start at null, iterate through
    {
      if (max == null || shapes.get(i).perimeter() > max.perimeter())
      // apart from first time where max is set to null when perimeter will be
      // greater; iterate through shapes, to obtain perimeter
      // where greater than i, return the max perimeter
      {
        max = shapes.get(i);
      }
    }
    return max.perimeter();
  }
}
